#!/bin/sh

#
#  Copyright	2022-2024	Echedelle López Romero
#  Copyright	2024		Chimmie Firefly
#
#  Licensed to the Apache Software Foundation (ASF) under one or more
#  contributor license agreements.  See the NOTICE file distributed with
#  this work for additional information regarding copyright ownership.
#  The ASF licenses this file to You under the Apache License, Version 2.0
#  (the "License"); you may not use this file except in compliance with
#  the License.  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#


SCRIPT_RPATH="$(realpath "$0")"

set -e

export USER="${USER:-root}"
export DEBIAN_FRONTEND="${DEBIAN_FRONTEND:-noninteractive}"
export TZ="${TZ:-UTC}"

if [ "$USER" != "root" ]; then
	sudo /bin/sh "$SCRIPT_RPATH"
	exit $?
fi

apt-get update
apt-get full-upgrade -y
apt-get autopurge -y
apt-get clean

apt-get install -y \
	net-tools dnsmasq \
	$(apt list | grep qemu-system | cut -f 1 -d '/' | tr '
' ' ') \
	libvirt-daemon-system libvirt-clients qemu-kvm qemu-utils \
	passwd

apt-get autopurge -y
apt-get clean

for DIR in $(find /home/* -maxdepth 0 -type d | rev | cut -f 1 -d '/' | rev); do
	usermod -aG libvirt "$DIR" || true
done

if [ -z "$SYSTEMD_DNSMASQ_FORBID_SHUT" ]; then
	systemctl disable dnsmasq || true
	systemctl stop dnsmasq || true
fi

systemctl enable libvirtd || true
systemctl start libvirtd || true
