#!/bin/sh

#
#  Copyright	2022-2024	Echedelle López Romero
#  Copyright	2024		Chimmie Firefly
#
#  Licensed to the Apache Software Foundation (ASF) under one or more
#  contributor license agreements.  See the NOTICE file distributed with
#  this work for additional information regarding copyright ownership.
#  The ASF licenses this file to You under the Apache License, Version 2.0
#  (the "License"); you may not use this file except in compliance with
#  the License.  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#


CONF_PATH="${CONF_PATH:-/etc/webdock-wakeup/var.conf}"

if [ -f "$CONF_PATH" ]; then
	. "$CONF_PATH"
fi

WEBDOCK_API_KEY="${WEBDOCK_API_KEY:-dummy}"
NODE_PREFIX="${NODE_PREFIX:-exitnode}"
AWAKE_NODES="${AWAKE_NODES:-0}"

CURRENT_DAY=$(date +%d)
CURRENT_HOUR=$(date +%H)

wakeup_node() {
	NODE_NAME="$1"
	NODE_AWAKE="$(curl -sL -4 -X GET -H \
		"Authorization: Bearer ${WEBDOCK_API_KEY}" \
		"https://api.webdock.io/v1/servers/${NODE_NAME}" | \
		jq -r .status)"
	case "$NODE_AWAKE" in
		stopped)
			curl -sL -4 -X POST -H \
				"Authorization: Bearer ${WEBDOCK_API_KEY}" \
				"https://api.webdock.io/v1/servers/${NODE_NAME}/actions/start"
			echo 'OK'
			;;
		*)
			echo "STATE ${NODE_AWAKE}"
			;;
	esac
}

wakeup_nodes() {
	for node_number in ${AWAKE_NODES}; do
		response="$(wakeup_node "${NODE_PREFIX}${node_number}")"
		case "$response" in
			OK)
				echo "[$(date)]: Node ${NODE_PREFIX}${node_number} awaken!"
				;;
			*)
				echo "[$(date)]: Node ${NODE_PREFIX}${node_number} returned ' ${response} '.."
				;;
		esac
	done
}

if [ $CURRENT_DAY -eq 1 ]; then
	if [ $CURRENT_HOUR -eq 0 ] || [ $CURRENT_HOUR -gt 0 ] && [ $CURRENT_HOUR -lt 2 ]; then
		wakeup_nodes
	fi
fi

if [ $CURRENT_DAY -eq 30 ] || [ $CURRENT_DAY -gt 30 ]; then
	if [ $CURRENT_HOUR -gt 22 ]; then
		wakeup_nodes
	fi
fi
