#!/bin/sh

#
#  Copyright	2022-2024	Echedelle López Romero
#  Copyright	2024		Chimmie Firefly
#
#  Licensed to the Apache Software Foundation (ASF) under one or more
#  contributor license agreements.  See the NOTICE file distributed with
#  this work for additional information regarding copyright ownership.
#  The ASF licenses this file to You under the Apache License, Version 2.0
#  (the "License"); you may not use this file except in compliance with
#  the License.  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#


SCRIPT_RPATH="$(realpath "$0")"

set -e

export USER="${USER:-root}"
export DEBIAN_FRONTEND="${DEBIAN_FRONTEND:-noninteractive}"
export TZ="${TZ:-UTC}"

if [ "$USER" != "root" ]; then
	sudo /bin/sh "$SCRIPT_RPATH"
	exit $?
fi

apt-get update
apt-get full-upgrade -y
apt-get autopurge -y
apt-get clean

apt-get install -y \
	net-tools restic \
	passwd cron

apt-get autopurge -y
apt-get clean

if [ -z "$RESTIC_AWS_CREDENTIALS_REGION" ]; then
	read -p "Enter your AWS region (default: nl-ams): " RESTIC_AWS_CREDENTIALS_REGION
fi
RESTIC_AWS_CREDENTIALS_REGION="${RESTIC_AWS_CREDENTIALS_REGION:-nl-ams}"

if [ -z "$RESTIC_AWS_CREDENTIALS_KEY_ID" ]; then
	read -p "Enter your AWS KEY ID: " RESTIC_AWS_CREDENTIALS_KEY_ID
fi

if [ -z "$RESTIC_AWS_CREDENTIALS_KEY_SECRET" ]; then
	printf "Enter your AWS KEY SECRET: "
	stty -echo
	read RESTIC_AWS_CREDENTIALS_KEY_SECRET
	stty echo
	printf "\n"
fi

if [ -z "$RESTIC_AWS_CREDENTIALS_GATEWAY" ]; then
	read -p "Enter your AWS gateway (default: s3.nl-ams.scw.cloud): " RESTIC_AWS_CREDENTIALS_GATEWAY
fi
RESTIC_AWS_CREDENTIALS_GATEWAY="${RESTIC_AWS_CREDENTIALS_GATEWAY:-s3.nl-ams.scw.cloud}"

if [ -z "$RESTIC_AWS_CREDENTIALS_BUCKET" ]; then
	read -p "Enter your AWS bucket name: " RESTIC_AWS_CREDENTIALS_BUCKET
fi

if [ -z "$RESTIC_AWS_STORAGE_CLASS" ]; then
	read -p "Enter your storage class (default: GLACIER): " RESTIC_AWS_STORAGE_CLASS
fi
RESTIC_AWS_STORAGE_CLASS="${RESTIC_AWS_STORAGE_CLASS:-GLACIER}"

mkdir -p /etc/restic 2>/dev/null 3>&2 || true

cat <<EOF > /etc/restic/env
export AWS_DEFAULT_REGION="$RESTIC_AWS_CREDENTIALS_REGION"
export AWS_ACCESS_KEY_ID="$RESTIC_AWS_CREDENTIALS_KEY_ID"
export AWS_SECRET_ACCESS_KEY="$RESTIC_AWS_CREDENTIALS_KEY_SECRET"
export RESTIC_REPOSITORY="s3:https://$RESTIC_AWS_CREDENTIALS_GATEWAY/$RESTIC_AWS_CREDENTIALS_BUCKET"
export RESTIC_PASSWORD_FILE=/etc/restic/passwd
export RESTIC_AWS_STORAGE_CLASS="$RESTIC_AWS_STORAGE_CLASS"

EOF

chmod 400 /etc/restic/env

RESTIC_PASSWORD="${RESTIC_PASSWORD:-$(< /dev/urandom tr -dc 'A-Za-z0-9' | head -c 64)}"

cat <<EOF > /etc/restic/passwd
$RESTIC_PASSWORD
EOF

chmod 400 /etc/restic/passwd

cat <<EOF'' > /etc/restic/excludes.txt

EOF

cat <<EOF'' > /etc/restic/includes.txt
/etc
/home
/root
/var

EOF

cat <<EOF'' > /usr/local/bin/restic-backups
#!/bin/sh

set -e # exit inmediately on command execution failure
set -u # treat unset variables as an error when substituting

BACKUP_FILE="/etc/restic/includes.txt"
EXCLUDE_FILE="/etc/restic/excludes.txt"
BACKUP_TAG="cron"

RETENTION_DAYS=15
RETENTION_WEEKS=2
RETENTION_MONTHS=1
RETENTION_YEARS=0

. /etc/restic/env

restic unlock &

wait $!

restic backup \
  --verbose \
  -o s3.storage-class=$RESTIC_AWS_STORAGE_CLASS \
  --one-file-system \
  --tag $BACKUP_TAG \
  --files-from $BACKUP_FILE \
  --exclude-file $EXCLUDE_FILE &

wait $!

restic forget \
  --verbose \
  -o s3.storage-class=$RESTIC_AWS_STORAGE_CLASS \
  --tag $BACKUP_TAG \
  --prune \
  --keep-daily $RETENTION_DAYS \
  --keep-weekly $RETENTION_WEEKS \
  --keep-monthly $RETENTION_MONTHS \
  --keep-yearly $RETENTION_YEARS &

wait $!

restic check &
wait $!

echo "Backup done!"

EOF

chmod +x /usr/local/bin/restic-backups

cat <<EOF'' > /etc/cron.daily/restic-backup
#!/bin/sh

set -e # exit inmediately on command execution failure
set -u # treat unset variables as an error when substituting

BACKUPS_DIRECTORY='/srv/dumps'

/usr/local/bin/restic-backups

EOF

chmod +x /etc/cron.daily/restic-backup

systemctl enable cron || true
systemctl start cron || true
