#!/bin/sh

#
#  Copyright	2022-2024	Echedelle López Romero
#  Copyright	2024		Chimmie Firefly
#
#  Licensed to the Apache Software Foundation (ASF) under one or more
#  contributor license agreements.  See the NOTICE file distributed with
#  this work for additional information regarding copyright ownership.
#  The ASF licenses this file to You under the Apache License, Version 2.0
#  (the "License"); you may not use this file except in compliance with
#  the License.  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#


SCRIPT_RPATH="$(realpath "$0")"

set -e

export USER="${USER:-root}"
export DEBIAN_FRONTEND="${DEBIAN_FRONTEND:-noninteractive}"
export TZ="${TZ:-UTC}"

if [ "$USER" != "root" ]; then
	sudo /bin/sh "$SCRIPT_RPATH"
	exit $?
fi

apt-get update
apt-get full-upgrade -y
apt-get autopurge -y
apt-get clean

apt-get update
apt-get install -y \
	systemd cron
apt-get autopurge -y
apt-get clean

apt-get update
apt-get install -y \
	apache2 apache2-utils \
	libapache2-mod-security2 \
	libapache2-mod-geoip \
	libapache2-mod-apparmor \
	certbot python3-certbot-apache \
	sed logrotate
apt-get autopurge -y
apt-get clean

cat <<EOF'' > /etc/modsecurity/modsecurity.conf
# -- Rule engine initialization ----------------------------------------------

# Enable ModSecurity, attaching it to every transaction. Use detection
# only to start with, because that minimises the chances of post-installation
# disruption.
#
SecRuleEngine DetectionOnly


# -- Request body handling ---------------------------------------------------

# Allow ModSecurity to access request bodies. If you don't, ModSecurity
# won't be able to see any POST parameters, which opens a large security
# hole for attackers to exploit.
#
SecRequestBodyAccess On


# Enable XML request body parser.
# Initiate XML Processor in case of xml content-type
#
SecRule REQUEST_HEADERS:Content-Type "^(?:application(?:/soap\+|/)|text/)xml" \
     "id:'200000',phase:1,t:none,t:lowercase,pass,nolog,ctl:requestBodyProcessor=XML"

# Enable JSON request body parser.
# Initiate JSON Processor in case of JSON content-type; change accordingly
# if your application does not use 'application/json'
#
SecRule REQUEST_HEADERS:Content-Type "^application/json" \
     "id:'200001',phase:1,t:none,t:lowercase,pass,nolog,ctl:requestBodyProcessor=JSON"

# Sample rule to enable JSON request body parser for more subtypes.
# Uncomment or adapt this rule if you want to engage the JSON
# Processor for "+json" subtypes
#
#SecRule REQUEST_HEADERS:Content-Type "^application/[a-z0-9.-]+[+]json" \
#     "id:'200006',phase:1,t:none,t:lowercase,pass,nolog,ctl:requestBodyProcessor=JSON"

# Maximum request body size we will accept for buffering. If you support
# file uploads then the value given on the first line has to be as large
# as the largest file you are willing to accept. The second value refers
# to the size of data, with files excluded. You want to keep that value as
# low as practical.
#
SecRequestBodyLimit 13107200
SecRequestBodyNoFilesLimit 131072

# Store up to 128 KB of request body data in memory. When the multipart
# parser reaches this limit, it will start using your hard disk for
# storage. That is slow, but unavoidable.
#
SecRequestBodyInMemoryLimit 131072

# What do do if the request body size is above our configured limit.
# Keep in mind that this setting will automatically be set to ProcessPartial
# when SecRuleEngine is set to DetectionOnly mode in order to minimize
# disruptions when initially deploying ModSecurity.
#
SecRequestBodyLimitAction Reject

# Maximum parsing depth allowed for JSON objects. You want to keep this
# value as low as practical.
#
SecRequestBodyJsonDepthLimit 512

# Verify that we've correctly processed the request body.
# As a rule of thumb, when failing to process a request body
# you should reject the request (when deployed in blocking mode)
# or log a high-severity alert (when deployed in detection-only mode).
#
SecRule REQBODY_ERROR "!@eq 0" \
"id:'200002', phase:2,t:none,log,deny,status:400,msg:'Failed to parse request body.',logdata:'%{reqbody_error_msg}',severity:2"

# By default be strict with what we accept in the multipart/form-data
# request body. If the rule below proves to be too strict for your
# environment consider changing it to detection-only. You are encouraged
# _not_ to remove it altogether.
#
SecRule MULTIPART_STRICT_ERROR "!@eq 0" \
"id:'200003',phase:2,t:none,log,deny,status:400, \
msg:'Multipart request body failed strict validation: \
PE %{REQBODY_PROCESSOR_ERROR}, \
BQ %{MULTIPART_BOUNDARY_QUOTED}, \
BW %{MULTIPART_BOUNDARY_WHITESPACE}, \
DB %{MULTIPART_DATA_BEFORE}, \
DA %{MULTIPART_DATA_AFTER}, \
HF %{MULTIPART_HEADER_FOLDING}, \
LF %{MULTIPART_LF_LINE}, \
SM %{MULTIPART_MISSING_SEMICOLON}, \
IQ %{MULTIPART_INVALID_QUOTING}, \
IP %{MULTIPART_INVALID_PART}, \
IH %{MULTIPART_INVALID_HEADER_FOLDING}, \
FL %{MULTIPART_FILE_LIMIT_EXCEEDED}'"

# Did we see anything that might be a boundary?
#
SecRule MULTIPART_UNMATCHED_BOUNDARY "!@eq 0" \
"id:'200004',phase:2,t:none,log,deny,msg:'Multipart parser detected a possible unmatched boundary.'"

# PCRE Tuning
# We want to avoid a potential RegEx DoS condition
#
SecPcreMatchLimit 100000
SecPcreMatchLimitRecursion 100000

# Some internal errors will set flags in TX and we will need to look for these.
# All of these are prefixed with "MSC_".  The following flags currently exist:
#
# MSC_PCRE_LIMITS_EXCEEDED: PCRE match limits were exceeded.
#
SecRule TX:/^MSC_/ "!@streq 0" \
        "id:'200005',phase:2,t:none,deny,msg:'ModSecurity internal error flagged: %{MATCHED_VAR_NAME}'"


# -- Response body handling --------------------------------------------------

# Allow ModSecurity to access response bodies. 
# You should have this directive enabled in order to identify errors
# and data leakage issues.
# 
# Do keep in mind that enabling this directive does increases both
# memory consumption and response latency.
#
SecResponseBodyAccess On

# Which response MIME types do you want to inspect? You should adjust the
# configuration below to catch documents but avoid static files
# (e.g., images and archives).
#
SecResponseBodyMimeType text/plain text/html text/xml

# Buffer response bodies of up to 512 KB in length.
SecResponseBodyLimit 524288

# What happens when we encounter a response body larger than the configured
# limit? By default, we process what we have and let the rest through.
# That's somewhat less secure, but does not break any legitimate pages.
#
SecResponseBodyLimitAction ProcessPartial


# -- Filesystem configuration ------------------------------------------------

# The location where ModSecurity stores temporary files (for example, when
# it needs to handle a file upload that is larger than the configured limit).
# 
# This default setting is chosen due to all systems have /tmp available however, 
# this is less than ideal. It is recommended that you specify a location that's private.
#
SecTmpDir /tmp/

# The location where ModSecurity will keep its persistent data.  This default setting 
# is chosen due to all systems have /tmp available however, it
# too should be updated to a place that other users can't access.
#
SecDataDir /tmp/


# -- File uploads handling configuration -------------------------------------

# The location where ModSecurity stores intercepted uploaded files. This
# location must be private to ModSecurity. You don't want other users on
# the server to access the files, do you?
#
#SecUploadDir /opt/modsecurity/var/upload/

# By default, only keep the files that were determined to be unusual
# in some way (by an external inspection script). For this to work you
# will also need at least one file inspection rule.
#
#SecUploadKeepFiles RelevantOnly

# Uploaded files are by default created with permissions that do not allow
# any other user to access them. You may need to relax that if you want to
# interface ModSecurity to an external program (e.g., an anti-virus).
#
#SecUploadFileMode 0600


# -- Debug log configuration -------------------------------------------------

# The default debug log configuration is to duplicate the error, warning
# and notice messages from the error log.
#
#SecDebugLog /opt/modsecurity/var/log/debug.log
#SecDebugLogLevel 3


# -- Audit log configuration -------------------------------------------------

# Log the transactions that are marked by a rule, as well as those that
# trigger a server error (determined by a 5xx or 4xx, excluding 404,  
# level response status codes).
#
SecAuditEngine RelevantOnly
SecAuditLogRelevantStatus "^(?:5|4(?!04))"

# Log everything we know about a transaction.
SecAuditLogParts ABDEFHIJZ

# Use a single file for logging. This is much easier to look at, but
# assumes that you will use the audit log only ocassionally.
#
SecAuditLogType Serial
SecAuditLog /var/log/apache2/modsec_audit.log

# Specify the path for concurrent audit logging.
#SecAuditLogStorageDir /opt/modsecurity/var/audit/


# -- Miscellaneous -----------------------------------------------------------

# Use the most commonly used application/x-www-form-urlencoded parameter
# separator. There's probably only one application somewhere that uses
# something else so don't expect to change this value.
#
SecArgumentSeparator &

# Settle on version 0 (zero) cookies, as that is what most applications
# use. Using an incorrect cookie version may open your installation to
# evasion attacks (against the rules that examine named cookies).
#
SecCookieFormat 0

# Specify your Unicode Code Point.
# This mapping is used by the t:urlDecodeUni transformation function
# to properly map encoded data to your language. Properly setting
# these directives helps to reduce false positives and negatives.
#
SecUnicodeMapFile unicode.mapping 20127

# Improve the quality of ModSecurity by sharing information about your
# current ModSecurity version and dependencies versions.
# The following information will be shared: ModSecurity version,
# Web Server version, APR version, PCRE version, Lua version, Libxml2
# version, Anonymous unique id for host.
# NB: As of April 2022, there is no longer any advantage to turning this
# setting On, as there is no active receiver for the information.
SecStatusEngine Off

EOF

mkdir -p "/etc/letsencrypt/live/localhost.dummy" 2>/dev/null 3>&2 || true
openssl req -x509 -newkey rsa:4096 \
	-keyout "/etc/letsencrypt/live/localhost.dummy/privkey.pem" \
	-out "/etc/letsencrypt/live/localhost.dummy/fullchain.pem" \
	-nodes \
	-subj "/C=IX/ST=Nya/L=Meow/O=Self hosted/OU=Self hosted/CN=localhost.dummy"

if ! [ -f /etc/apache2/dhparams.pem ]; then
	openssl dhparam -out /etc/apache2/dhparams.pem 4096
fi

cat <<EOF'' > /etc/apache2/sites-available/001-localhost.dummy.conf
<VirtualHost *:80>

	ServerName localhost.dummy

	Protocols h2c http/1.1

	RewriteEngine On
	RewriteCond %{SERVER_NAME} =localhost.dummy
	RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]

	ErrorLog ${APACHE_LOG_DIR}/localhost-dummy_error.log
	CustomLog ${APACHE_LOG_DIR}/localhost-dummy_access.log combined

</VirtualHost>

<IfModule mod_ssl.c>

	SSLStaplingCache shmcb:/tmp/stapling_cache(128000)

	<VirtualHost *:443>

		ServerName localhost.dummy

		Protocols h2 http/1.1

		RequestHeader setifempty X-Forwarded-Proto expr=%{REQUEST_SCHEME}

		ProxyPreserveHost On
		SSLProxyEngine On
		ProxyPass / https://127.0.0.1:6969/ upgrade=websocket timeout=30 retry=0 keepalive=On
		ProxyPassReverse / https://127.0.0.1:6969/

		Include /etc/letsencrypt/options-ssl-apache.conf

		SSLCertificateFile /etc/letsencrypt/live/localhost.dummy/fullchain.pem
		SSLCertificateKeyFile /etc/letsencrypt/live/localhost.dummy/privkey.pem

        SSLOpenSSLConfCmd DHParameters "/etc/apache2/dhparams.pem"

		SSLUseStapling On

		ErrorLog ${APACHE_LOG_DIR}/localhost-dummy-ssl_error.log
		CustomLog ${APACHE_LOG_DIR}/localhost-dummy-ssl_access.log combined
		
	</VirtualHost>

</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet

EOF

a2ensite 001-localhost.dummy.conf

systemctl reload apache2 || true
systemctl restart apache2 || true

cat <<EOF'' > /usr/local/bin/a2enable-website
#!/bin/sh

SCRIPT_RPATH="$(realpath "$0")"

set -e

export DOMAIN="${DOMAIN:-$1}"
export FORWARD_HOST="${FORWARD_HOST:-$2}"
export EMAIL="${EMAIL:-$3}"

export DOMAIN="${DOMAIN:-localhost.dummy}"
export FORWARD_HOST="${FORWARD_HOST:-127.0.0.1:6969}"
export EMAIL="${EMAIL:-gameplayer2019pl@tutamail.com}"

export USER="${USER:-root}"

if [ "$USER" != "root" ]; then
	sudo /bin/sh "$SCRIPT_RPATH"
	exit $?
fi

domain_enumerate() {
	value=$(expr $(ls /etc/apache2/sites-available/*.conf | tr ' ' '\n' | wc -l) - 1)
	value_length=$(echo -n "$value" | wc -m)
	out=""
	for _i in $(seq 1 $(expr 3 - $value_length)); do
		out="${out}0"
	done || true
	echo "${out}${value}"
}

__FILE_NAME="$(domain_enumerate)-${DOMAIN}.conf"

cp /etc/apache2/sites-available/001-localhost.dummy.conf "/etc/apache2/sites-available/${__FILE_NAME}"

sed -i "s/localhost.dummy/${DOMAIN}/g" "/etc/apache2/sites-available/${__FILE_NAME}"
sed -i "s/localhost-dummy/$(echo -n "${DOMAIN}" | tr '.' '-')/g" "/etc/apache2/sites-available/${__FILE_NAME}"
sed -i "s/127.0.0.1:6969/${FORWARD_HOST}/g" "/etc/apache2/sites-available/${__FILE_NAME}"

a2ensite "${__FILE_NAME}" || true

certbot --apache -d "${DOMAIN}" \
    --non-interactive --agree-tos \
	--email "${EMAIL}"

systemctl reload apache2 || true

EOF

chmod +x /usr/local/bin/a2enable-website

cat <<EOF'' > /usr/local/bin/a2disable-website
#!/bin/sh

SCRIPT_RPATH="$(realpath "$0")"

set -e

export DOMAIN="${DOMAIN:-$1}"

export DOMAIN="${DOMAIN:-localhost.dummy}"

export USER="${USER:-root}"

if [ "$USER" != "root" ]; then
	sudo /bin/sh "$SCRIPT_RPATH"
	exit $?
fi

a2dissite "${DOMAIN}" || true

systemctl reload apache2 || true

certbot delete --cert-name "${DOMAIN}" || true

cd /etc/apache2/sites-available

find . -name "*-${DOMAIN}.conf" | xargs -P 1 -I '{}' rm -f "{}" 

systemctl reload apache2 || true

EOF

chmod +x /usr/local/bin/a2disable-website

cat <<EOF'' > /etc/logrotate.d/apache2
/var/log/apache2/*.log {
	daily
	missingok
	rotate 7
	size 16M
	compress
	delaycompress
	notifempty
	create 640 root adm
	sharedscripts
	prerotate
		if [ -d /etc/logrotate.d/httpd-prerotate ]; then
			run-parts /etc/logrotate.d/httpd-prerotate
		fi
	endscript
	postrotate
		if pgrep -f ^/usr/sbin/apache2 > /dev/null; then
			invoke-rc.d apache2 reload 2>&1 | logger -t apache2.logrotate
		fi
	endscript
}

EOF

logrotate -f /etc/logrotate.d/apache2 || true
